﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {
    public Vector3 startPoint;
    public Vector3 endPoint;
    public float speed = 1.0f;
    private bool movingForward = true;

	// Use this for initialization
	void Start () {
        transform.position = startPoint;
	}

    // Update is called once per frame
    void Update()
    {
        Vector3 target;

        if (movingForward)
        {
            target = endPoint;
        }
        else
        {
            target = startPoint;
        }

        float distanceToMove = speed * Time.deltaTime;
        float distanceToTarget =
            (target - transform.position).magnitude;
        Debug.Log("distance to target =" + distanceToTarget);
        Debug.Log("distance to move =" + distanceToMove);

        if (distanceToMove > distanceToTarget)
        {
            transform.position = target;
            movingForward = !movingForward;
            Debug.Break();
        }

        else
        {
            Vector3 dir = (target - transform.position).normalized;
            transform.position += dir * distanceToMove;
        }


    }
}
